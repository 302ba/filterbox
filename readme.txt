FilterBox 
Version 2.3
Copyright (c) 1999-2015 Sergiy Kurinny

Only for Delphi XE8. Other Delphi 
versions are supported in v2.03 (D5-D7)

e-mail: kurin@gudinfo.org
        sergiy.kurinny@outlook.com	
	sergiy.ivanov.2014@gmail.com

   WWW: soft.gudinfo.org

FilterBox is a freeware components library 
for the developers working with Delphi. 

Includes 

- different components allowing to implement 
powerful but easy to use data filtering, sorting 
and search features in your applications.

- highly customizable and top quality calendar 
and date-time editing controls which support 
different appearance, themes and styles.

- great looking controls which allows to 
implement state of the art color and font 
picking.

======================================

Older versions of this component were known
as puterSoft.SDK. PuterSoft company was closed
by me some time ago and SDK was made freeware.
Now I had time to update components
to Delphi XE8 and also made some improvements.

                         Sergiy Kurinny